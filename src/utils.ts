const LOGO_FILE_TYPES = ['.svg'];
const BACKGROUND_FILE_TYPES = ['.jpeg', '.jpg', '.png'];

function isImageFile(file: File): boolean {
  return !!file.type.match(/image.*/);
}

function isLogoFile(file: File): boolean {
  return !!file.type.match(/image\/svg\+xml/);
}

function readFile(file: File, callback: (data) => void): void {
  const reader = new FileReader();
  reader.addEventListener(
    'load',
    function () {
      callback(reader.result);
    },
    false,
  );

  reader.readAsDataURL(file);
}

export const utils = {
  LOGO_FILE_TYPES,
  BACKGROUND_FILE_TYPES,
  isImageFile,
  isLogoFile,
  readFile,
};
