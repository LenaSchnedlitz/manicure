import { fireEvent, render, screen, waitFor } from '@testing-library/svelte';
import TitleEditor from '../../components/TitleEditor.svelte';

const testProps = { onTitleChange: jest.fn(), onColorChange: jest.fn(), titleColor: 'rgb(255, 0, 0)' };

describe('TitleEditor Component', () => {
  it('should render an textbox with configured text color', () => {
    render(TitleEditor, { ...testProps });

    const inputField = screen.getByRole('textbox');
    expect(inputField).toBeVisible();
    expect(inputField.style.color).toBe(''); // color is set on parent
    expect(inputField.parentElement.style.color).toBe(testProps.titleColor);
  });

  it('should _NOT_ render color picker when textbox is empty', () => {
    render(TitleEditor, { ...testProps });

    expect(screen.getByTitle('Title Color')).not.toBeVisible();
  });

  it('should render color picker with configured color when textbox is filled', async () => {
    render(TitleEditor, { ...testProps });

    const inputField = screen.getByRole('textbox');
    await fireEvent.change(inputField, { target: { value: 'New Title' } });

    const colorPicker = screen.getByTitle('Title Color');
    await waitFor(() => expect(colorPicker).toBeVisible());
    expect(screen.getByTestId('fake-color-picker').style.color).toBe(testProps.titleColor);
  });

  it('should run `onTitleChange` when textbox value changes', async () => {
    const mockChangeHandler = jest.fn();
    render(TitleEditor, { ...testProps, onTitleChange: mockChangeHandler });

    const inputField = screen.getByRole('textbox');
    await fireEvent.change(inputField, { target: { value: 'New Title' } });

    expect(mockChangeHandler).toHaveBeenCalled();
  });

  it('should run `onColorChange` when color picker value changes', async () => {
    const mockChangeHandler = jest.fn();
    render(TitleEditor, { ...testProps, onColorChange: mockChangeHandler });

    const colorPicker = screen.getByTitle('Title Color');
    await fireEvent.change(colorPicker, { target: { value: '#0000ff' } });

    expect(mockChangeHandler).toHaveBeenCalled();
  });
});
