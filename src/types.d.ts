export interface CustomCanvas {
  export: (number: string, type: string) => void;
  redraw: () => void;
}
