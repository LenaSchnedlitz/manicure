import App from './App.svelte';

const app = new App({
  target: document.body,
  props: {
    dimensions: {
      rectangle: {
        width: 1280,
        height: 640,
        logoSize: 256,
      },
      logoBig: 512,
      logoSmall: 192,
      favicon: 128,
    },
  },
});

export interface RectangleDimensions {
  width: number;
  height: number;
  logoSize: number;
}

export interface Dimensions {
  rectangle: RectangleDimensions;
  logoBig: number;
  logoSmall: number;
  favicon: number;
}

export default app;
