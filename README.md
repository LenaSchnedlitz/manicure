<div align="center">

![Logo](public/logo-192.png)

# Manicure

#### Generate preview pics the easy way.

[![Made with Svelte](https://shields.io/badge/Made_with-Svelte-5199e0?style=flat)](https://svelte.dev/)
[![Hosted on GitHub Pages](https://shields.io/badge/Hosted_on-GitHub_Pages-894adf?style=flat)](https://github.com/LenaSchnedlitz/manicure)
[![Probably works](https://shields.io/badge/Probably-works-dc389a?style=flat)]()
[![Try it out here](https://shields.io/badge/Try_it_out-»_here_«-ee3377?style=flat)](https://www.lenaschnedlitz.me/manicure)
[![License MIT](https://shields.io/badge/License-MIT-f05869?style=flat)](#license)

[Key Features](#key-features) · [Installation](#installation) · [How to Use](#how-to-use) · [Contributing](#contributing) · [License](#license)

![Screenshot](preview.png)

</div>

## Key Features

- Generate various PNGs from your (SVG-)logo
- Find a background for your social share preview pic
- Add a title and/or short description
- Download all files - ready to be copied into your project

[Try it out!](https://www.lenaschnedlitz.me/manicure)

## Installation

To clone and run this application, you need [Git](https://git-scm.com) and [npm](https://npmjs.com).
Run from your command line:

```shell
# Clone the repository
$ git clone git@gitlab.com:LenaSchnedlitz/manicure.git

# Go into the freshly downloaded directory
$ cd manicure

# Install dependencies
$ npm i
```

## How to Use

This application was built with [Svelte](https://svelte.dev/). To start development mode, follow the [installation steps described above](#installation) and run (from your command line):

```shell
$ npm run dev
```

For production builds, run:

```shell
# Build the application
$ npm run build

# Start the application from latest build
$ npm run start
```

## Contributing

Merge requests and bug reports are welcome. I will try my best to process them as soon as possible, but please be patient - my spare time is (sadly) very limited.

## License

[MIT](LICENSE) · Credits are much appreciated :heart:
